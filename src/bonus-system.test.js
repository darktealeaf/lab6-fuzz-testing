import {calculateBonuses} from "./bonus-system.js";
const assert = require("assert");

describe('Bonus system tests', () => {
  test('Standart and amount less 10000', () => {
    const program = "Standard";
    const amount = 5000;

    expect(calculateBonuses(program, amount)).toEqual(0.05);
  });

  test('Standart and amount 100000', (done) => {
    const program = "Standard";
    const amount = 100000;

    assert.equal(calculateBonuses(program, amount),2.5*0.05);
    done();
  });

  test('Diamond and amount 10000', (done) => {
    const program = "Diamond";
    const amount = 10000;

    assert.equal(calculateBonuses(program, amount),0.2*1.5);
    done();
  });

  test('Premium and amount 50000', (done) => {
    const program = "Premium";
    const amount = 50000;

    assert.equal(calculateBonuses(program, amount),0.1*2);
    done();
  });

  test('Premium and amount bigger 100000', () => {
    const program = "Premium";
    const amount = 101000;

    expect(calculateBonuses(program, amount)).toEqual(0.25);
  });

  test('Invalid program and valid amount', () => {
    const program = "tiohr";
    const amount = 18000;

    expect(calculateBonuses(program, amount)).toEqual(0);
  });

  test('Invalid both parameters', () => {
    const program = true;
    const amount = "wef";

    expect(calculateBonuses(program, amount)).toEqual(0);
  });

  test('Empty both parameters', () => {
    const program = "";
    const amount = "";

    expect(calculateBonuses(program, amount)).toEqual(0);
  });
})
